(in-package :pg)

(defmethod s-sql:sql-type-name ((lisp-type (eql 'timestamptz)) &rest args)
  (cond (args (format nil "TIMESTAMPTZ(~A)" (car args)))
        (t "TIMESTAMPTZ")))

(defparameter *sql-type-to-lisp-type-alist*
  '(("integer" . integer)
    ("boolean" . boolean)
    ("timestamp with time zone" . timestamptz)))

(defun to-lisp-type (sql-type)
  (declare (type string sql-type))
  "Transform a formatted sql type into a lisp type that
can be used in a dao-class"
  (or
   (cdr (assoc sql-type *sql-type-to-lisp-type-alist* :test #'string-equal))
   (error "Don't know how to translate ~S into a lisp type" sql-type)))
