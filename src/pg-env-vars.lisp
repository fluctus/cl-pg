(defpackage pg/env-vars
  (:use :cl))
(in-package :pg/env-vars)

(defmacro make-pg-var-getter (var &key (process 'progn) default (bind 'var))
  (let* ((env
           (concatenate 'string "PG" var))
         (getter-name
           (intern (concatenate 'string "GET" env))))
    `(defun ,getter-name ()
       (let ((,bind (uiop:getenv ,env)))
         (if ,bind (,process ,bind) ,default)))))

(make-pg-var-getter "USER" :default "postgres")
(make-pg-var-getter "PASSWORD" :default "")
(make-pg-var-getter "HOST" :default "localhost")
(make-pg-var-getter "PORT" :process parse-integer :default 5432)
(make-pg-var-getter "DATABASE" :default "")
(make-pg-var-getter "SSLCERT" :default "~/.postgresql/postgres.crt")
(make-pg-var-getter "SSLKEY" :default "~/.postgresql/postgres.key")
(make-pg-var-getter "SSLROOTCERT" :default "~/.postgresql/root.crt")
(make-pg-var-getter "SSLMODE" :default "prefer")
(make-pg-var-getter "APPNAME")

(eval-when (:load-toplevel)
  (do-symbols (sym)
    (let ((name (symbol-name sym)))
      (when (and
             (eql (symbol-package sym) *package*)
             (string-equal "GETPG" (subseq name 0 (min 5 (length name))))
             (fboundp sym))
        (export sym)))))
