(defpackage pg
  (:use :cl :pg/env-vars)
  (:import-from :postmodern #:query #:execute #:with-transaction)
  (:export #:connect
           #:disconnect
           #:connected-p
           #:with-database
           #:query
           #:execute
           #:with-transaction
           #:to-lisp-type
           #:timestamptz
           #:copy-sql-reader))
(in-package :pg)

; ============== Internal defs =================

(defun sslmode-translate (sslmode)
  (cond
    ((string-equal sslmode "prefer") :try)
    ((string-equal sslmode "verify-ca") :yes)
    ((string-equal sslmode "verify-full") :full)
    (t :no)))

(defparameter +cl+ssl-disabled-protocols+
  (list
   cl+ssl:+ssl-op-no-sslv2+
   cl+ssl:+ssl-op-no-sslv3+
   cl+ssl:+ssl-op-no-tlsv1+
   cl+ssl:+ssl-op-no-tlsv1-1+))

(defun get-ssl-context (sslrootcert)
  (cl+ssl:make-context
   :disabled-protocols +cl+ssl-disabled-protocols+
   :verify-location sslrootcert))

; ============== Exported API ================

(defun connect
    (&key
       (user (getpguser)) (password (getpgpassword))
       (host (getpghost)) (port (getpgport))
       (dbname (getpgdatabase)) (appname (getpgappname))
       (sslcert (getpgsslcert)) (sslkey (getpgsslkey))
       (sslrootcert (getpgsslrootcert)) (sslmode (getpgsslmode)))
  (let ((postmodern::*ssl-certificate-file* sslcert)
        (postmodern::*ssl-key-file* sslkey)
        (use-ssl (sslmode-translate (unless (eq host :unix) sslmode)))
        database)
    (cl+ssl:with-global-context
        ((unless (eq use-ssl :no) (get-ssl-context sslrootcert))
         :auto-free-p (not (eq use-ssl :no)))
      (setf database (postmodern:connect dbname user password host
                                         :port port :use-ssl use-ssl)))
    (when appname
      (cl-postgres:exec-query
       database
       (format nil "SET application_name TO ~a"
               (s-sql:sql-escape-string appname))))
    database))

(defun disconnect (database)
  (when (connected-p database)
    (postmodern:disconnect database)))

(defun connected-p (database)
  "Wrapper around postmodern:connected-p that also handles nil inputs"
  (and database (postmodern:connected-p database)))

(defmacro with-database (database &body body)
  `(let ((postmodern:*database* ,database))
     ,@body))

; =========== Data Types Support ===================

(defun copy-sql-reader (to-oid from-oid &optional (table cl-postgres:*sql-readtable*))
  (let ((reader (gethash from-oid table (cl-postgres::get-type-interpreter :default))))
    (unless (null reader)
      (setf (gethash to-oid table) reader))))
