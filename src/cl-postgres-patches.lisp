(in-package :cl-postgres)

(define-interpreter oid:+char+ "char" ((value int 1))
  (code-char value))

; =========== Connection Overrides: Timeout Support ===========

(defclass database-connection ()
  ((host :initarg :host :reader connection-host)
   (port :initarg :port :reader connection-port)
   (database :initarg :db :reader connection-db)
   (user :initarg :user :reader connection-user)
   (password :initarg :password :reader connection-password)
   (use-ssl :initarg :ssl :reader connection-use-ssl)
   (use-binary :initarg :binary :accessor connection-use-binary :initform nil)
   (service :initarg :service :accessor connection-service)
   (application-name :initarg :application-name :accessor connection-application-name)
   (timeout :initarg :timeout :accessor connection-timeout)
   (socket :initarg :socket :accessor connection-socket)
   (meta :initform nil)
   (available :initform t :accessor connection-available)
   (parameters :accessor connection-parameters)
   (timestamp-format :accessor connection-timestamp-format))
  (:default-initargs :application-name "postmodern-default")
  (:documentation "Representation of a database connection. Contains
login information in order to be able to automatically re-establish a
connection when it is somehow closed."))

(defun open-database (database user password host
                      &optional (port 5432) (use-ssl :no)
                        (service "postgres") (application-name "postmodern-default")
                        (use-binary nil) (timeout nil))
  "Create and open a connection for the specified server, database, and user.
use-ssl may be :no, :try, :yes, or :full; where :try means 'if the server
supports it'. :require uses provided ssl certificate with no verification.
:yes only verifies that the server cert is issued by a trusted CA,
but does not verify the server hostname. :full 'means expect a CA-signed cert
for the supplied host name' and verify the server hostname. When it is anything
but :no, you must have the CL+SSL package loaded to initiate the connection.

On SBCL and Clozure CL, the value :unix may be passed for host, in order to
connect using a Unix domain socket instead of a TCP socket."
  (check-type database string)
  (check-type user string)
  (check-type password (or null string))
  (check-type host (or string (eql :unix)) "a string or :unix")
  (check-type port (integer 1 65535) "an integer from 1 to 65535")
  (check-type use-ssl (member :no :try :require :yes :full) ":no, :try, :require, :yes or :full")
  (let ((conn (make-instance 'database-connection :host host :port port
                                                  :user user :password password
                                                  :socket nil :db database
                                                  :ssl use-ssl :timeout timeout
                                                  :binary use-binary
                                                  :service service
                                                  :application-name application-name))
        (connection-attempts 0))
    (initiate-connection conn connection-attempts)
    conn))

#+(and (or cl-postgres.features:sbcl-available ccl allegro) unix)
(progn
  #+cl-postgres.features:sbcl-available
  (defun unix-socket-connect (path &optional timeout)
    (let ((sock (make-instance 'sb-bsd-sockets:local-socket :type :stream)))
      (sb-bsd-sockets:socket-connect sock path)
      (sb-bsd-sockets:socket-make-stream
       sock :input t :output t :element-type '(unsigned-byte 8) :timeout timeout)))

  #+ccl
  (defun unix-socket-connect (path &optional timeout)
    (ccl:make-socket :type :stream
                     :address-family :file
                     :format :binary
                     :remote-filename path
                     :input-timeout timeout :output-timeout timeout :connect-timeout timeout))

  #+allegro
  (defun unix-socket-connect (path &optional timeout)
    (socket:make-socket :type :stream
                        :address-family :file
                        :format :binary
                        :remote-filename path)))

#+cl-postgres.features:sbcl-available
(defun inet-socket-connect (host port &optional timeout)
  (let* ((host-ent (get-host-address host))
         (sock (make-instance
                #+cl-postgres.features:sbcl-ipv6-available
                (ecase (sb-bsd-sockets:host-ent-address-type host-ent)
                  (2  'sb-bsd-sockets:inet-socket)
                  (10 'sb-bsd-sockets:inet6-socket))

                #-cl-postgres.features:sbcl-ipv6-available
                'sb-bsd-sockets:inet-socket

                :type :stream :protocol :tcp))
         (address (sb-bsd-sockets:host-ent-address host-ent)))
    (sb-bsd-sockets:socket-connect sock address port)
    (sb-bsd-sockets:socket-make-stream
     sock :input t :output t :buffering :full :element-type '(unsigned-byte 8) :timeout timeout)))

#+ccl
(defun inet-socket-connect (host port &optional timeout)
  (when (and (stringp host)
             (string= host "localhost"))
    (setf host "127.0.0.1")) ;this corrects a strange ccl error we are seeing in certain scram authentication situations
  (ccl:make-socket :format :binary
                   :remote-host host
                   :remote-port port
                   :input-timeout timeout :output-timeout timeout :connect-timeout timeout))

#+allegro
(defun inet-socket-connect (host port &optional timeout)
  (socket:make-socket :remote-host host
                      :remote-port port
                      :format :binary
                      :type :stream))

(defun initiate-connection (conn &optional (connection-attempts 0))
  "Check whether a connection object is connected, try to connect it
if it isn't."
  (flet ((add-restart (err)
           (restart-case (error (wrap-socket-error err))
             (:reconnect () :report "Try again."
               (progn (incf connection-attempts)
                      (initiate-connection conn connection-attempts)))))
         (assert-unix ()
           #+unix t
           #-unix (error "Unix sockets only available on Unix (really)")))
    (handler-case
        (let ((socket #-(or allegro cl-postgres.features:sbcl-available ccl)
                      (usocket:socket-stream
                       (usocket:socket-connect (connection-host conn)
                                               (connection-port conn)
                                               :element-type '(unsigned-byte 8)
                                               :timeout (connection-timeout conn)))
                      #+(or allegro cl-postgres.features:sbcl-available ccl)
                      (cond
                        ((equal (connection-host conn) :unix)
                         (assert-unix)
                         (unix-socket-connect (unix-socket-path *unix-socket-dir*
                                                                (connection-port conn))
                                              (connection-timeout conn)))
                        ((and (stringp (connection-host conn))
                              (char= #\/ (aref (connection-host conn) 0)))
                         (assert-unix)
                         (unix-socket-connect (unix-socket-path (connection-host conn)
                                                                (connection-port conn))
                                              (connection-timeout conn)))
                        ((and (pathnamep (connection-host conn))
                              (eql :absolute (pathname-directory (connection-host conn))))
                         (assert-unix)
                         (unix-socket-connect (unix-socket-path (namestring (connection-host conn))
                                                                (connection-port conn))
                                              (connection-timeout conn)))
                        (t
                         (inet-socket-connect (connection-host conn)
                                              (connection-port conn)
                                              (connection-timeout conn)))))
              (finished nil)
              (*connection-params* (make-hash-table :test 'equal)))
          (setf (connection-parameters conn) *connection-params*)
          (unwind-protect
               (setf socket (handler-case
                                (authenticate socket conn)
                              (cl-postgres-error:protocol-violation (err)
                                (declare (ignorable err))
                                (setf finished t)
                                (ensure-socket-is-closed socket)
                                ;; If we settled on a single logging library, I
                                ;; would suggest logging this kind of situation
                                ;; with at least the following data
                                ;; (database-error-message err)
                                ;; (database-error-detail err)
                                (incf connection-attempts)
                                (when (< connection-attempts
                                         *retry-connect-times*)
                                  (initiate-connection conn
                                                       connection-attempts))))
                     (connection-timestamp-format conn)
                     (if (string= (gethash "integer_datetimes"
                                           (connection-parameters conn)) "on")
                         :integer :float)
                     (connection-socket conn) socket
                     finished t)
            (unless finished
              (ensure-socket-is-closed socket)))
          (maphash (lambda (id query-param-list)
                     (prepare-query conn id
                                    (first query-param-list)
                                    (second query-param-list)))
                   (connection-meta conn)))
      #-(or allegro cl-postgres.features:sbcl-available ccl)
      (usocket:socket-error (e) (add-restart e))
      #+ccl (ccl:socket-error (e) (add-restart e))
      #+allegro(excl:socket-error (e) (add-restart e))
      #+cl-postgres.features:sbcl-available(sb-bsd-sockets:socket-error (e) (add-restart e))
      #+cl-postgres.features:sbcl-available(sb-bsd-sockets:name-service-error (e) (add-restart e))
      (stream-error (e) (add-restart e))))
  (values))
