(defsystem "pg"
  :version "0.2.0"
  :depends-on ("uiop" "cl+ssl" "postmodern" "cl-postgres+local-time")
  :components ((:module "src"
                :serial t
                :components
                ((:file "cl-postgres-patches")
                 (:file "postmodern-patches")
                 (:file "pg-env-vars")
                 (:file "pg")
                 (:file "types"))))
  :description "A thin convenience wrapper around postmodern"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.org")))
